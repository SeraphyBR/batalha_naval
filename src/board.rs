#![allow(dead_code)]

use std::fmt;
use crate::ship::ShipOrietation;

use super::ship::Ship;
use super::utils::digits;

/// Representa o tabuleiro do jogo Batalha-naval
pub struct Board {
    /// Matrix que pode conter um Navio ou não
    matrix: Vec<Vec<Option<Ship>>>,
    /// Matrix contendo valores booleanos para representar
    /// os tiros que não acertaram um Navio
    missed: Vec<Vec<bool>>
}

impl Board {
    /// Inicializa um tabuleiro vazio, com as dimensões informadas
    pub fn empty(height: usize, width: usize) -> Board {
        Board {
            matrix: vec![vec![Option::None; width]; height],
            missed: vec![vec![false; width]; height]
        }
    }

    /// Insere um navio no tabuleiro
    pub fn insert_ship(&mut self, ship: Ship, x: usize, y: String) -> Result<(), &str> {
        let y = Board::get_position_from(y)?;
        if x >= self.matrix[0].len() || y >= self.matrix.len() {
            return Err("Erro ao inserir, posição invalida!");
        }

        let parts = ship.split_parts();
        match ship.orietation {
            ShipOrietation::Horizontal => {
                let mut nx = x;
                for part in parts {
                    self.matrix[y][nx] = Some(part);
                    nx += 1;
                }
            },
            ShipOrietation::Vertical => {
                let mut ny = y;
                for part in parts {
                    self.matrix[ny][x] = Some(part);
                    ny += 1;
                }
            }
        }
        Ok(())
    }

    /// Responsável por marcar no tabuleiro um tiro,
    /// caso for um *hit*, é retornado o navio/parte que foi atingido.
    pub fn make_shot(&mut self, x: usize, y: String) -> Result<Option<&Ship>, &str>{
        let y = Board::get_position_from(y)?;
        if let Some(ship) = &mut self.matrix[y][x] {
            ship.piece_hit = true;
            Ok(Some(ship))
        }
        else {
            self.missed[y][x] = true;
            Ok(None)
        }
    }

    /// Verifica se um navio afundou
    ///
    /// E esperado que informe uma parte do navio, e sua posição no tabuleiro,
    /// para determinar a posição das demais peças e conferir se foram todas atingidas.
    pub fn is_ship_sunk(&self, ship: &Ship, x: usize, y: String) -> bool {
        let y = Board::get_position_from(y).unwrap();
        let mut is_sunk = false;
        match ship.orietation {
            ShipOrietation::Horizontal => {
                let mut nx = x - ship.piece_id;
                loop {
                    if nx >= self.matrix[y].len() {
                        break is_sunk
                    }
                    if let Some(s) = &self.matrix[y][nx] {
                        if s.piece_hit && s.is_part_of(&ship) {
                            is_sunk = true;
                        }
                        else if !s.piece_hit {
                            is_sunk = false;
                        }
                        else {
                            break is_sunk;
                        }
                    } else {
                        break is_sunk;
                    }
                    nx += 1;
                }
            },
            ShipOrietation::Vertical => {
                let mut ny = y - ship.piece_id;
                loop {
                    if ny >= self.matrix.len() {
                        break is_sunk
                    }
                    if let Some(s) = &self.matrix[ny][x] {
                        if s.piece_hit && s.is_part_of(&ship) {
                            is_sunk = true;
                        }
                        else if !s.piece_hit {
                            is_sunk = false;
                        }
                        else {
                            break is_sunk;
                        }
                    } else {
                        break is_sunk;
                    }
                    ny += 1;
                }
            }
        }
    }

    /// Responsável por converter uma coordenada de valor ASCII para númerico
    fn get_position_from(y: String) -> Result<usize, &'static str> {
        let mut pos = 0;
        let begin = 'a' as u8;
        let mut decimal_place = 1;
        for c in y.to_lowercase().as_bytes() {
            if c.is_ascii_alphabetic() {
                let v = (c % (begin - 1)) as usize;
                pos += v * decimal_place;
            }
            else {
                return Err("Posição invalida!");
            }
            decimal_place *= 10;
        }
        Ok(pos - 1)
    }

    /// Responsável por converter um valor númerico para a coordenada em ASCII.
    ///
    /// Utilizada para a exibição do tabuleiro
    fn get_y_label(y: usize) -> String {
        let mut s = vec![];
        let inicial = 'a' as u8;
        for d in digits(y) {
            let v = d as u8;
            s.push(v + inicial);
        }
        String::from_utf8(s).unwrap().to_uppercase()
    }
}

impl fmt::Display for Board {
    /// Implementa a trait Display, para exibição do tabuleiro
    ///
    /// O tabuleiro será exibido com o seguinte formato:
    ///
    /// ``` rust
    /// println!("{}", Board::empty(10, 10));
    /// ```
    /// <pre>
    ///   0  1  2  3  4  5  6  7  8  9
    ///A  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    ///B  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    ///C  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    ///D  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    ///E  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    ///F  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    ///G  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    ///H  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    ///I  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    ///J  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?
    /// </pre>
    ///
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "  ")?;
        let digits = format!("{}", self.matrix.len()).len();
        let v_align = " ".repeat(digits - 1);
        for x in 0..self.matrix.len() {
            write!(f, "{}{} ", v_align, x)?;
        }
        writeln!(f, "")?;
        let mut y = 0;
        for j in self.matrix.as_slice() {
            write!(f, "{} ", Board::get_y_label(y))?;
            let mut x = 0;
            for element in j {
                write!(f, "{}", v_align)?;
                match element {
                    Some(s) => {
                        if s.piece_hit {
                            write!(f, "◉ ")?;
                        } else {
                            write!(f, "{:?} ", s)?;
                        }
                    },
                    None => {
                        if self.missed[y][x] {
                            write!(f, "○ ")?;
                        }
                        else {
                            write!(f, "? ")?;
                        }
                    }
                }
                x += 1;
            }
            y += 1;
            writeln!(f, "")?;
        }
        Ok(())
    }
}