#![allow(dead_code)]

use std::fmt;
use uuid::Uuid;


/// Representa os tipos de navios que existem
#[derive(Clone, PartialEq)]
pub enum ShipType {
    Aerocarrier,
    Tanker,
    Destroyer,
    Submarine,
    Unknow
}

/// Representa a informação de orientação do navio
#[derive(Clone, PartialEq)]
pub enum ShipOrietation {
    Horizontal,
    Vertical
}

/// Representa um Navio do jogo
#[derive(Clone)]
pub struct Ship {
    id: Uuid,
    stype: ShipType,
    pub piece_id: usize,
    pub piece_hit: bool,
    pub orietation: ShipOrietation,
}

impl Ship {
    /// Cria um novo navio
    ///
    /// ## Argumentos
    ///
    /// * `stype` - O tipo de navio a ser criado
    /// * `orietation` - A orientação do navio
    pub fn new(stype: ShipType, orietation: ShipOrietation) -> Self {
        Self {
            id: Uuid::new_v4(),
            piece_id: 0,
            piece_hit: false,
            stype,
            orietation
        }
    }

    /// Serve para criar um navio "desconhecido", é usado quando um navio inimigo foi atingido
    /// e salvamos a posição do tiro no tabuleiro
    ///
    /// ## Argumentos
    ///
    /// * `hit` - Indica se o navio ou parte dele, foi atingido
    pub fn unknow(hit: bool) -> Self {
        let mut s = Ship::new(ShipType::Unknow, ShipOrietation::Horizontal);
        s.piece_hit = hit;
        s
    }

    /// Determina se uma parte de um navio faz parte de outra
    pub fn is_part_of(&self, other: &Ship) -> bool {
        self.id == other.id &&
        self.stype == other.stype &&
        self.orietation == other.orietation
    }

    /// Serve para obter o número de quadrados que um navio ocupa,
    /// com base no seu tipo
    ///
    /// * Porta-Aviões: 5
    /// * Navios-tanque: 4
    /// * Contratorperdeiros: 3
    /// * Submarinos: 2
    pub fn squares(&self) -> usize {
        match self.stype {
            ShipType::Aerocarrier => 5,
            ShipType::Tanker => 4,
            ShipType::Destroyer => 3,
            ShipType::Submarine => 2,
            _ => 1
        }
    }

    /// Irá dividir um navio em suas partes, com base no seu tipo
    ///
    /// Retorna um vetor com as partes do navio
    pub fn split_parts(&self) -> Vec<Ship> {
        let mut parts = Vec::new();
        for i in 0..self.squares() {
            let mut npart = self.clone();
            npart.piece_id = i;
            parts.push(npart);
        }
        parts
    }
}

impl fmt::Debug for Ship {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.stype {
            ShipType::Aerocarrier => write!(f, "a"),
            ShipType::Tanker => write!(f, "t"),
            ShipType::Destroyer => write!(f, "d"),
            ShipType::Submarine => write!(f, "s"),
            ShipType::Unknow => write!(f, "u")
        }
    }
}