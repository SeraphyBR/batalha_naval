use std::error::Error;
use std::io::{BufWriter, BufReader};
use std::net::{SocketAddr, TcpListener, TcpStream, Ipv4Addr};

mod board;
mod ship;
mod game;
mod utils;

use utils::read;

use human_panic::setup_panic;
use serde;
use serde::Deserialize;

use game::{Game, GameParameters};

/// Porta de entrada da aplicação
fn main() -> Result<(), Box<dyn Error>> {
    setup_panic!();
    first_menu();
    Ok(())
}

/// O menu principal do jogo, com as opções:
/// * 1 - Iniciar novo jogo, você será o servidor
/// * 2 - Entrar em um jogo, você será o cliente
/// * 3 - Sair
fn first_menu() {
    loop {
        println!("--- Batalha Naval ---");
        println!("Você deseja:");
        println!("1 - Iniciar novo jogo, você será o servidor");
        println!("2 - Entrar em um jogo, você será o cliente");
        println!("3 - Sair\n");
        match read("OP: ").trim() {
            "1" => {
                start_new_game();
                println!("Jogo encerrado!");
            }
            "2" => {
                connect_to_game();
                println!("Jogo encerrado!");
            },
            "3" => {
                println!("Jogo encerrado!");
                break;
            },
            _ => continue,
        }
    }
}

/// Responsável por iniciar um servidor, perguntado pelo nome do jogador,
/// e a porta de conexão a ser usada, com valor de porta padrão 8080.
///
/// Um servidor TCP será iniciado no endereço 127.0.0.1 da máquina.
fn start_new_game() {
    let username = read("Informe o seu nome de jogador: ");
    let port: u16 = read("Defina a porta de conexão a ser usada [8080]: ")
                        .trim()
                        .parse()
                        .unwrap_or_else(|_|{println!("A porta padrão 8080 será usada!"); 8080});

    let addr = SocketAddr::from(([127, 0, 0, 1], port));

    let mut game_parameters = ask_game_parameters();
    game_parameters.username_server = username;

    let listener = TcpListener::bind(addr).unwrap();

    for stream in listener.incoming() {
        match stream {
            Ok(s) => {
                handle_server_side(s, game_parameters.clone());
                break;
            },
            Err(e) => eprintln!("{}", e),
        }
    }
}

/// Responsável por iniciar um cliente TCP, perguntando pelo nome do jogador,
/// o ip do servidor e a porta utilizada.
///
/// Irá tentar se conectar a um servidor.
fn connect_to_game() {
    let username = read("Informe o seu nome de jogador: ");
    let ip_addr: Ipv4Addr = read("Informa o IP do servidor: ")
                            .trim()
                            .parse()
                            .unwrap();
    let port: u16 = read("Informe a porta definida pelo servidor: ")
                        .trim()
                        .parse()
                        .unwrap();

    let addr = SocketAddr::from((ip_addr, port));
    loop {
        if let  Ok(s) = TcpStream::connect(addr){
            println!("Conectado com sucesso!");
            handle_client_side(s, username);
            break;
        }
        else {
            println!("Não foi possivel se conectar a ");
            println!("Deseja tentar novamente? [s/n]");
            match read("OP: ").trim(){
                "s" => continue,
                "n" => break,
                _ => continue,
            }
        }
    };

}

/// Lida com a troca de mensagens inicial, no lado do cliente.
///
/// Aqui o cliente obtem do servidor os parametros do jogo.
fn handle_client_side(stream: TcpStream, username: String) {
    let reader = BufReader::new(stream.try_clone().unwrap());
    let writer = BufWriter::new(stream.try_clone().unwrap());

    let mut de = serde_json::Deserializer::from_reader(reader);
    let mut params = GameParameters::deserialize(&mut de).unwrap();
    params.username_client = username;
    serde_json::to_writer(writer, &params).unwrap();
    let mut game = Game::new(params, false);
    game.start(stream);
}

/// Lida com a troca de mensagens inicial, no lado do servidor.
///
/// Aqui o servidor informa o cliente que se conectou, dos parametros do jogo.
fn handle_server_side(stream: TcpStream, game_parameters: GameParameters) {
    let reader = BufReader::new(stream.try_clone().unwrap());
    let writer = BufWriter::new(stream.try_clone().unwrap());

    println!("Alguem está se conectando!, informando parametros do jogo...");
    serde_json::to_writer(writer, &game_parameters).unwrap();

    let mut de = serde_json::Deserializer::from_reader(reader);
    let params = GameParameters::deserialize(&mut de).unwrap();
    let mut game = Game::new(params, true);
    game.start(stream);
}

/// Pergunta ao jogador (servidor) se deseja alterar algum parametro do jogo
fn ask_game_parameters() -> GameParameters {
    let will_configure = match read("Deseja configurar parametros do jogo [s/n]? ").trim() {
        "s" => true,
        _ => false
    };

    let mut parameters = GameParameters::default();

    if will_configure {

        if let Ok(value) = read("Quantidade de Porta-avioes: ").trim().parse() {
            parameters.max_aerocarriers = value;
        }
        else {
            println!("O valor padrão será usado!");
        }

        if let Ok(value) = read("Quantidade de Navios-tanque: ").trim().parse() {
            parameters.max_tankers = value;
        }
        else {
            println!("O valor padrão será usado!");
        }

        if let Ok(value) = read("Quantidade de Contratorperdeiros: ").trim().parse() {
            parameters.max_destroyers = value;
        }
        else {
            println!("O valor padrão será usado!");
        }

        if let Ok(value) = read("Quantidade de Submarinos: ").trim().parse() {
            parameters.max_submarines = value;
        }
        else {
            println!("O valor padrão será usado!");
        }

        if let Ok(value) = read("Largura do tabuleiro: ").trim().parse() {
            parameters.board_width = value;
        }
        else {
            println!("O valor padrão será usado!");
        }

        if let Ok(value) = read("Altura do tabuleiro: ").trim().parse() {
            parameters.board_height = value;
        }
        else {
            println!("O valor padrão será usado!");
        }

        parameters
    }
    else {
        parameters
    }
}
