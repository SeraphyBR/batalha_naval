#![allow(dead_code)]

use std::io::{BufWriter, BufReader};
use std::net::TcpStream;

use serde::{Serialize, Deserialize};

use crate::ship::{Ship, ShipOrietation, ShipType};

use super::board::Board;
use super::utils::read;

/// Representa a instancia de um jogo/partida
pub struct Game {
    /// Parametros do jogo
    parameters: GameParameters,
    /// Representa o tabuleiro do Cliente
    client_board: Board,
    /// Representa o tabuleiro do Servidor
    server_board: Board,
    /// Contador de navios inimigos afundados
    enemy_sunks: usize,
    /// Contem o número de navios inimigos
    enemy_ships: usize,
    /// Determina se a instancia atual do jogo é
    /// do servidor, caso contrário, é do cliente
    is_server: bool,
}

impl Game {
    /// Cria um novo jogo, com os parâmetros informados
    pub fn new(parameters: GameParameters, is_server: bool) -> Game {
        let width = parameters.board_width;
        let height = parameters.board_height;
        Game {
            parameters,
            client_board: Board::empty(height, width),
            server_board: Board::empty(height, width),
            enemy_sunks: 0,
            enemy_ships: 0,
            is_server
        }
    }

    /// Dá inicio ao jogo, apenas chama o método correspondente
    /// ao cliente/servidor
    pub fn start(&mut self, stream: TcpStream) {
        if self.is_server {
            self.start_server(stream);
        }
        else {
            self.start_client(stream);
        }
    }

    /// Dá inicio ao jogo na parte do servidor
    fn start_server(&mut self, stream: TcpStream) {
        let reader = BufReader::new(stream.try_clone().unwrap());
        let writer = BufWriter::new(stream.try_clone().unwrap());
        let num_ships = self.edition_board_menu();

        let mut ready = GameReady {
            server: true,
            num_server_ships: num_ships,
            client: false,
            num_client_ships: 0
        };
        serde_json::to_writer(writer, &ready).unwrap();
        println!("Aguardando o outro jogador...");
        let mut de = serde_json::Deserializer::from_reader(reader);
        ready = GameReady::deserialize(&mut de).unwrap();
        self.enemy_ships = ready.num_client_ships;
        if ready.server && ready.client {
            self.action_menu(stream);
        }
    }

    /// Dá inicio ao jogo na parte do cliente
    fn start_client(&mut self, stream: TcpStream) {
        let reader = BufReader::new(stream.try_clone().unwrap());
        let writer = BufWriter::new(stream.try_clone().unwrap());

        let num_ships = self.edition_board_menu();

        println!("Aguardando o outro jogador...");
        let mut de = serde_json::Deserializer::from_reader(reader);
        let mut ready: GameReady = GameReady::deserialize(&mut de).unwrap();
        ready.client = true;
        ready.num_client_ships = num_ships;
        serde_json::to_writer(writer, &ready).unwrap();
        self.enemy_ships = ready.num_server_ships;
        if ready.server && ready.client {
            self.action_menu(stream);
        }
    }

    /// Inicia a fase de edição do tabuleiro, apresentando um menu
    /// com as opções:
    /// * Mostrar meu tabuleiro
    /// * Adicionar navios
    /// * Finalizar edição do tabuleiro
    fn edition_board_menu(&mut self) -> usize {
        let board = if self.is_server {
            &mut self.server_board
        } else {
            &mut self.client_board
        };

        let mut counter = [0; 4];

        println!("Fase de edição do tabuleiro");
        loop {
            println!("O que deseja fazer: ");
            println!("1 - Mostrar meu tabuleiro");
            println!("2 - Adicionar navios");
            println!("3 - Finalizar edição do tabuleiro");
            match read("OP: ").trim() {
                "1" => {
                    println!("{}", board);
                },
                "2" => {
                    Game::ask_ships_to_insert(board, &mut counter, &self.parameters);
                },
                "3" => {
                    break;
                },
                _ => {}
            }
        }
        counter.iter().sum()
    }

    /// Função responsável por apresentar um menu para inserção de navios no tabuleiro
    fn ask_ships_to_insert(board: &mut Board, counter: &mut [usize], gparams: &GameParameters) {
        loop {
            if Game::is_full_of_ships(counter, gparams) {
                println!("Todos os navios já foram inseridos!");
                break;
            }

            println!("Qual o tipo de navio: ");
            println!("1 - Porta-Aviões [{}/{}]", counter[0], gparams.max_aerocarriers);
            println!("2 - Navio-tanque [{}/{}]", counter[1], gparams.max_tankers);
            println!("3 - Contratorperdeiro [{}/{}]", counter[2], gparams.max_destroyers);
            println!("4 - Submarino [{}/{}]", counter[3], gparams.max_submarines);
            let stype = loop {
                match read("OP: ").trim() {
                    "1" => {
                        if counter[0] < gparams.max_aerocarriers {
                            counter[0] += 1;
                            break ShipType::Aerocarrier
                        } else {
                            println!("Limite máximo atingido!");
                            continue
                        }
                    },
                    "2" => {
                        if counter[1] < gparams.max_tankers {
                            counter[1] += 1;
                            break ShipType::Tanker
                        } else {
                            println!("Limite máximo atingido!");
                            continue
                        }
                    },
                    "3" => {
                        if counter[2] < gparams.max_destroyers {
                            counter[2] += 1;
                            break ShipType::Destroyer
                        } else {
                            println!("Limite máximo atingido!");
                            continue
                        }
                    },
                    "4" => {
                        if counter[3] < gparams.max_submarines {
                            counter[3] += 1;
                            break ShipType::Submarine
                        } else {
                            println!("Limite máximo atingido!");
                            continue
                        }
                    },
                    _ => {}
                }
            };
            let orietation = loop {
                match read("Orientação [h/v]: ").to_lowercase().trim() {
                    "h" => break ShipOrietation::Horizontal,
                    "v" => break ShipOrietation::Vertical,
                    _ => {}
                }
            };

            let ship = Ship::new(stype, orietation);

            let pos = Game::read_board_position();

            let success_insert = match board.insert_ship(ship, pos.0, pos.1) {
                Ok(_) => true,
                Err(e) => {
                    println!("{}", e);
                    false
                }
            };

            if success_insert {
                match read("Adicionar outro navio [s/n]? ").to_lowercase().trim() {
                    "s" => continue,
                    _ => break,
                }
            }
        }
    }

    /// Representa o menu de ações do jogo, é quando ocorre a maior adrenalina.
    ///
    /// Onde o jogo realmente acontece, toda ação enviada, tem um resultado retornado
    /// pelo cliente/servidor, vice-versa
    fn action_menu(&mut self, stream: TcpStream) {
        let reader = BufReader::new(stream.try_clone().unwrap());
        let writer = BufWriter::new(stream.try_clone().unwrap());

        // Quem começa é o lado do servidor, logo o cliente aguarda resposta
        if !self.is_server {
            // Espero a ação do oponente
            println!("Aguardando jogada do oponente...");
            let mut de = serde_json::Deserializer::from_reader(reader);
            let enemy_action: GameAction = GameAction::deserialize(&mut de).unwrap();
            if enemy_action == GameAction::Surrender {
                println!("O inimigo se rendeu!, o jogo foi encerrado.");
                return;
            }
            // Envio o resultado da ação ao oponente
            let result = self.handle_action(enemy_action);
            serde_json::to_writer(writer, &result).unwrap();
        }

        loop {
            let reader = BufReader::new(stream.try_clone().unwrap());
            let writer = BufWriter::new(stream.try_clone().unwrap());
            let reader2 = BufReader::new(stream.try_clone().unwrap());
            let writer2 = BufWriter::new(stream.try_clone().unwrap());
            println!("O que deseja fazer: ");
            println!("1 - Mostrar meu tabuleiro");
            println!("2 - Mostrar tabuleiro inimigo");
            println!("3 - Fazer um tiro");
            println!("4 - Se render");
            match read("OP: ").trim() {
                "1" => {
                    if self.is_server {
                        println!("{}", self.server_board);
                    } else {
                        println!("{}", self.client_board);
                    }
                },
                "2" => {
                    println!("{} Navios inimigos abatidos", self.enemy_sunks);
                    if self.is_server {
                        println!("{}", self.client_board);
                    } else {
                        println!("{}", self.server_board);
                    }
                },
                "3" => {
                    // Envio minha ação
                    let pos = Game::read_board_position();
                    let action = GameAction::Shot(pos.0, pos.1);
                    serde_json::to_writer(writer, &action).unwrap();

                    // Recebo a resposta da minha ação
                    let mut de = serde_json::Deserializer::from_reader(reader);
                    let result: GameActionResult = GameActionResult::deserialize(&mut de).unwrap();
                    self.handle_action_result(action, result);

                    if self.enemy_sunks >= self.enemy_ships {
                        println!("Você venceu!!!");
                        break;
                    }


                    // Espero a ação do oponente
                    println!("Aguardando jogada do oponente...");
                    let mut de = serde_json::Deserializer::from_reader(reader2);
                    let enemy_action: GameAction = GameAction::deserialize(&mut de).unwrap();
                    if enemy_action == GameAction::Surrender {
                        println!("O inimigo se rendeu!");
                        break;
                    }
                    // Envio o resultado da ação ao oponente
                    let result = self.handle_action(enemy_action);
                    serde_json::to_writer(writer2, &result).unwrap();
                },
                "4" => {
                    let action = GameAction::Surrender;
                    serde_json::to_writer(writer, &action).unwrap();
                    break;
                },
                _ => {}
            }
        }
    }

    /// Verifica se já foi inserido o número máximo de navios, de cada tipo.
    fn is_full_of_ships(counter: &[usize], gparams: &GameParameters) -> bool {
        counter[0] == gparams.max_aerocarriers &&
        counter[1] == gparams.max_tankers &&
        counter[2] == gparams.max_destroyers &&
        counter[3] == gparams.max_submarines
    }

    /// Responsável por perguntar ao usuário uma posição do tabuleiro
    fn read_board_position() -> (usize, String) {
        loop {
            let input = read("Posição ('A,0'): ").to_uppercase();
            let input: Vec<&str> = input.trim().split(',').collect();

            if input.len() != 2 {
                println!("Posição invalida!");
                continue;
            }

            let y = String::from(input[0]);
            let x: usize = match input[1].parse() {
                Ok(value) => value,
                Err(_) => {
                    println!("O segundo valor deve ser um número!");
                    continue;
                }
            };
            break (x,y);
        }
    }

    /// Método responsável por processar o resultado de uma ação que foi retornado
    /// pelo servidor/cliente.
    ///
    /// Caso o cliente tenha enviado uma ação ao servidor, o servidor responde com o resultado,
    /// o Cliente obtem o resultado, e então é processada aqui.
    fn handle_action_result(&mut self, action: GameAction, result: GameActionResult) {
        let enemy_board = if self.is_server {
            &mut self.client_board
        } else {
            &mut self.server_board
        };

        if let GameAction::Shot(x, y) = action {
            match result {
                GameActionResult::ShotHit => {
                    let ship = Ship::unknow(true);
                    enemy_board.insert_ship(ship, x, y).unwrap();
                },
                GameActionResult::ShotSunk => {
                    self.enemy_sunks += 1;
                    let ship = Ship::unknow(true);
                    enemy_board.insert_ship(ship, x, y).unwrap();
                },
                GameActionResult::ShotMissed => {
                    enemy_board.make_shot(x, y).unwrap();
                },
                _ => {}
            }
        }
    }

    /// Responsável por processar uma ação que foi recebida e retorna o resultado dela.
    fn handle_action(&mut self, action: GameAction) -> GameActionResult {
        let r = self.handle_action_p1(action.clone());
        self.handle_action_p2(action, r.0, r.1)
    }

    /// Parte 1 do *handle_action* que executa de fato a ação
    fn handle_action_p1(&mut self, action: GameAction) -> (GameActionResult, Option<Ship>) {
        if let GameAction::Shot(x, y) = action {
            let is_ship_hit = if self.is_server {
                self.server_board.make_shot(x, y.clone()).unwrap()
            } else {
                self.client_board.make_shot(x, y.clone()).unwrap()
            };
            if let Some(s) = is_ship_hit {
                (GameActionResult::ShotHit, Some(s.clone()))
            }
            else {
                (GameActionResult::ShotMissed, None)
            }
        } else {
            (GameActionResult::None, None)
        }
    }

    /// Parte 2 do *handle_action* que apenas verifica se ocorreu um hit,<br/>
    /// e caso positivo, irá verificar se o navio for afundado, retornando
    /// o devido resultado da ação
    fn handle_action_p2(&self, a: GameAction, r: GameActionResult, s: Option<Ship>) -> GameActionResult {
        if r == GameActionResult::ShotHit {
            if let Some(s) = s {
                if let GameAction::Shot(x, y) = a {
                    if self.is_server {
                        if self.server_board.is_ship_sunk(&s, x, y) {
                            return GameActionResult::ShotSunk;
                        }
                    } else {
                        if self.client_board.is_ship_sunk(&s, x, y) {
                            return GameActionResult::ShotSunk;
                        }
                    }
                }
            }
        }
        r
    }
}

/// Representa o tipo de ação que pode ser feita no jogo,
/// usado durante a comunicação cliente-servidor, ambos os lados
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum GameAction {
    /// Representa a ação de um tiro, possui como valores
    /// internos uma posição do tabuleiro
    Shot(usize, String),
    /// Representa a ação de rendição
    Surrender,
}

/// Representa o resultado de uma ação que foi realizada no jogo
/// usado durante a comunicação cliente-servidor, ambos os lados
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum GameActionResult {
    ShotHit,
    ShotSunk,
    ShotMissed,
    None
}

/// Estrutura usada para determinar se ambos os lados cliente e servidor
/// estão prontos para iniciar o jogo.
///
/// Contém tambem a informação do numero de navios de cada um.
#[derive(Serialize, Deserialize, Debug)]
pub struct GameReady {
    pub server: bool,
    pub num_server_ships: usize,
    pub client: bool,
    pub num_client_ships: usize,
}

/// Representa os parametros do jogo
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GameParameters {
    pub max_aerocarriers: usize,
    pub max_tankers: usize,
    pub max_destroyers: usize,
    pub max_submarines: usize,
    pub board_width: usize,
    pub board_height: usize,
    pub username_server: String,
    pub username_client: String,
}

impl GameParameters {
    /// Obtem os parâmetros padrões do jogo
    ///
    /// * Número máximo de Porta-Aviões: 1
    /// * Número máximo de Navios-tanque: 2
    /// * Número máximo de Contratorperdeiros: 3
    /// * Número máximo de Submarinos: 4
    /// * Largura do tabuleiro: 10 quadrados
    /// * Altura do tabuleiro: 10 quadrados
    /// * Nome do jogador do servidor: Vazio, a ser preenchido depois
    /// * Nome do jogador do cliente: Vazio, a ser preenchido depois
    pub fn default() -> Self {
        GameParameters {
            max_aerocarriers: 1,
            max_tankers: 2,
            max_destroyers: 3,
            max_submarines: 4,
            board_width: 10,
            board_height: 10,
            username_server: String::new(),
            username_client: String::new()
        }
    }
}