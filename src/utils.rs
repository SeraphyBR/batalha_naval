use std::io;
use std::io::Write;

/// Essa função serve para printar uma mensagem sem '\n',
/// e ler entrada do teclado
///
/// ## Argumentos
/// * `msg` - Uma string literal contendo a mensagem a ser exibida
pub fn read(msg: &str) -> String {
    let mut input = String::new();
    print!("{}", msg);
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut input)
        .expect("Erro ao ler do teclado!");
    input
} //Fim read()


/// Essa função retorna um iterator com os digitos de um número
///
/// ## Argumentos
///
/// * `num` - Um número inteiro
pub fn digits(mut num: usize) -> impl Iterator<Item = usize> {
    let mut divisor = 1;
    while num >= divisor * 10 {
        divisor *= 10;
    }

    std::iter::from_fn(move || {
        if divisor == 0 {
            None
        } else {
            let v = num / divisor;
            num %= divisor;
            divisor /= 10;
            Some(v)
        }
    })
}